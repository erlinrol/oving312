// @flow

import { Categories, Articles, sync } from '../src/models.js';

class Article {
  id: number;
  title: string;
  content: string;
  picture: string;
  category: string;
  importance: number;
  createdAt: string;
  updatedAt: string;
}

beforeAll(async () => {
  await sync;
});

describe('Articles test', () => {
  it('Correct data', async () => {
    let categories = await Categories.findAll();
    let articles = await Articles.findAll();

    expect(
      articles.map(article => article.toJSON()).map(article => ({
        id: article.id,
        title: article.title,
        picture: article.picture,
        category: article.category,
        importance: article.importance
      }))
    ).toEqual([
      {
        id: 1,
        title: 'Something about waffles being healthy',
        picture: 'https://tinyurl.com/y8chu5ro',
        category: 'Lifestyle',
        importance: 1
      },
      {
        id: 2,
        title: "Something about a cow but it's really a dog",
        picture: 'https://i.imgur.com/QQLuAQd.jpg',
        category: 'Humor',
        importance: 2
      },
      {
        id: 3,
        title: 'Illegal shoelaces',
        picture: 'https://tinyurl.com/y9kxukjp',
        category: 'Lifestyle',
        importance: 1
      }
    ]);
  });
});

describe('Articles test', () => {
  it('Get one', async () => {
    let article = null;
    article = await Articles.findOne({
      where: { id: 2 }
    });
    expect({
      article
    }).not.toEqual({
      article: null
    });
  });
});

describe('Articles test', () => {
  it('Create article', async () => {
    let articlesFirst = await Articles.findAll();

    await Articles.create({
      title: 'Illegal shoelaces',
      content: "Please don't buy these",
      picture: 'https://tinyurl.com/y9kxukjp',
      category: 'Lifestyle',
      importance: 1
    });

    let articlesAfter = await Articles.findAll();

    expect(articlesFirst.length).not.toEqual(articlesAfter.length);
  });
});

describe('Articles test', () => {
  it('Update article', async () => {
    let rowsAffected = await Articles.update(
      {
        title: 'test title',
        content: 'test content',
        picture: 'test picture',
        category: 'test category',
        importance: 2
      },
      {
        where: { id: 2 }
      }
    );

    expect(rowsAffected.length).toEqual(1);
  });
});

describe('Articles test', () => {
  it('Delete article', async () => {
    let rowsAffected = await Articles.destroy({
      where: { id: 2 }
    });

    expect(rowsAffected).toEqual(1);
  });
});

describe('Stupid test, jest working', () => {
  it('1 equals 1', async () => {
    expect(1).toEqual(1);
  });
});
