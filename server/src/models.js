// @flow

import Sequelize from 'sequelize';
import type { Model } from 'sequelize';

let sequelize = new Sequelize('School', 'root', '', {
  host: process.env.CI ? 'mysql' : 'localhost', // The host is 'mysql' when running in gitlab CI
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

export let Categories: Class<Model<{ id?: number, type: string }>> = sequelize.define('Categories', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  type: Sequelize.STRING
});

export let Articles: Class<
  // $FlowFixMe
  Model<{ id?: number, title: string, content: string, picture: string, category: string, importance: number }>
> = sequelize.define('Articles', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  title: Sequelize.STRING,
  content: Sequelize.TEXT,
  picture: Sequelize.TEXT,
  category: Sequelize.STRING,
  importance: Sequelize.INTEGER
});

// Drop tables and create test data when not in production environment
let production = process.env.NODE_ENV === 'production';

// The sync promise can be used to wait for the database to be ready (for instance in your tests)
export let sync = sequelize.sync({ force: production ? false : true }).then(() => {
  if (!production) return testData();
});

async function testData() {
  await Categories.create({
    type: 'Entertainment'
  });
  await Categories.create({
    type: 'Humor'
  });
  await Categories.create({
    type: 'Lifestyle'
  });
  await Categories.create({
    type: 'Politics'
  });
  await Categories.create({
    type: 'Sports'
  });

  await Categories.create({
    type: 'Science'
  });

  // await Articles.create({
  //   id: 4,
  //   title: 'Trump tests his new bat',
  //   content: 'Maybe the best tradedeal I have ever made',
  //   picture: 'https://tinyurl.com/ybznmxop',
  //   category: 'Lifestyle',
  //   importance: 1
  // });
  //
  // await Articles.create({
  //   id: 5,
  //   title: 'Elongated Muskrat',
  //   content: 'The one and only. Very Rich. Very smart.',
  //   picture: 'https://tinyurl.com/y7zp8obu',
  //   category: 'Lifestyle',
  //   importance: 1
  // });
  //
  // await Articles.create({
  //   id: 6,
  //   title: 'Norwegian propaganda',
  //   content: 'Beware parents!',
  //   picture: 'https://i.ytimg.com/vi/Cx4nxr14b-s/maxresdefault.jpg',
  //   category: 'Politics',
  //   importance: 1
  // });

  return (
    await Articles.create({
      title: 'Something about waffles being healthy',
      content: 'Trust me they are super health!',
      picture: 'https://tinyurl.com/y8chu5ro',
      category: 'Lifestyle',
      importance: 1
    }),
    await Articles.create({
      title: "Something about a cow but it's really a dog",
      content: 'He says Moof Moof',
      picture: 'https://i.imgur.com/QQLuAQd.jpg',
      category: 'Humor',
      importance: 2
    }),
    await Articles.create({
      title: 'Illegal shoelaces',
      content: "Please don't buy these",
      picture: 'https://tinyurl.com/y9kxukjp',
      category: 'Lifestyle',
      importance: 1
    })
  );
}
