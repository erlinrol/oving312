// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { ArticleCard } from '../src/components/articleCard.js';
import { shallow, mount } from 'enzyme';

describe('ArticleCard tests', () => {
  const wrapper = shallow(<ArticleCard articleId={1} title='title' picture='' category='Politics' content='content' date='2018-11-20T21:08:41Z'/>);

  it('initially', () => {
    let instance: ?ArticleCard = ArticleCard.instance();
    expect(typeof instance).toEqual('object');
    if (instance) expect(instance.optionsOpen).toEqual(false);
  });

});
