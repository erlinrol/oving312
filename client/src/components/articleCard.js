// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Alert } from '../widgets';
import { articleService, Category, Article } from '../services';

//dateformat rounds to minute
function getDate(date) {
  var dateObject = new Date(date);
  dateObject.setSeconds(0, 0);
  return dateObject
    .toISOString()
    .replace('T', ' ')
    .replace(':00.000Z', '');
}

export class ArticleCard extends Component<{
  articleId: number,
  title: string,
  date: string,
  picture: string,
  category: string
}> {
  toggle = this.toggle.bind(this);
  optionsOpen: boolean = false;

  render() {
    return (
      <div className="card article">
        <img className="card-img-top article-image" src={this.props.picture} alt="Card image" />
        <div className="card-body article-body">
          <NavLink to={'/article/' + this.props.articleId}>
            <h5 className="card-title article-title">{this.props.title}</h5>
          </NavLink>
          <h6 className="card-text article-category">{this.props.category}</h6>
          <h6 className="card-text article-date">{getDate(this.props.date)}</h6>

          <Dropdown className="article-options" direction="up" isOpen={this.optionsOpen} toggle={this.toggle}>
            <DropdownToggle color="info" caret>
              options
            </DropdownToggle>
            <DropdownMenu>
              <NavLink to={'/article/' + this.props.articleId + '/edit'}>
                <DropdownItem style={{ color: 'teal' }}>Edit</DropdownItem>
              </NavLink>
              <DropdownItem style={{ color: 'red' }} onClick={() => this.delete(this.props.articleId)}>
                Delete
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    );
  }

  toggle() {
    this.optionsOpen ? (this.optionsOpen = false) : (this.optionsOpen = true);
  }

  delete(id: number) {
    articleService
      .deleteArticle(id)
      .then(window.location.reload())
      .catch((error: Error) => Alert.danger(error.message));
  }
}
