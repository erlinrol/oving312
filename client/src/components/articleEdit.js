// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert } from '../widgets';
import { articleService, Category, Article } from '../services';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

export class ArticleEdit extends Component<{ match: { params: { id: number } } }> {
  // $FlowFixMe flowbug: doesn't understand article is Article without initial typechecking
  article: Article = null;
  categories: Category[] = [];

  render() {
    if (!this.article) return null;

    return (
      <div id="register">
        <Alert />
        <div id="register-wrapper">
          <div id="register-form" className="card">
            <form style={{ margin: '10px' }}>
              <div style={{ textAlign: 'center' }}>
                <h5 className="card-title">Edit article</h5>
              </div>
              <div className="form-group">
                <label style={{ float: 'left' }}>Title</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.article.title}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.article.title = event.target.value)
                  }
                />
              </div>
              <div className="form-group">
                <label style={{ float: 'left' }}>Picture URL</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.article.picture}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.article.picture = event.target.value.trim())
                  }
                />
              </div>
              <div className="form-group">
                <label>Text</label>
                <textarea
                  className="form-control"
                  rows="10"
                  value={this.article.content}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.article.content = event.target.value)
                  }
                />
              </div>

              <label>Category</label>
              <select
                className="selectpicker browser-default custom-select"
                defaultValue={this.article.category}
                onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                  (this.article.category = event.target.value)
                }
              >
                {this.categories.map(category => (
                  <option key={category.id} value={category.type}>
                    {category.type}
                  </option>
                ))}
              </select>
              <label style={{ marginTop: '20px' }}>Importance</label>
              <br />
              <div className="form-check importance">
                <input
                  className="form-check-input"
                  type="radio"
                  name="exampleRadios"
                  value="1"
                  checked={this.article.importance == 1 ? 'checked' : ''}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.article.importance = Number(event.target.value))
                  }
                />
                <label className="form-check-label">Important</label>
              </div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="exampleRadios"
                  value="2"
                  checked={this.article.importance == 2 ? 'checked' : ''}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.article.importance = Number(event.target.value))
                  }
                />
                <label className="form-check-label">Less Important</label>
              </div>
              <button
                type="button"
                className="btn btn-primary"
                style={{ marginTop: '20px' }}
                onClick={() => this.save()}
              >
                Save
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }

  save() {
    var valid = true;
    if (this.article.title == '') {
      valid = false;
      Alert.danger('Title required');
    } else if (this.article.title.length > 64) {
      valid = false;
      Alert.danger('Max title characters: 64');
    }
    if (this.article.category.trim() == '') {
      valid = false;
      Alert.danger('Category required');
    }
    if (this.article.importance != 1 && this.article.importance != 2) {
      valid = false;
      Alert.danger('Importance required');
    }

    if (valid) {
      if (this.article.picture.trim() == '') this.article.picture = 'https://tinyurl.com/y73nxqn9';
      articleService
        .updateArticle(this.article)
        .then(history.replace('/'), window.location.reload())
        .catch((error: Error) => Alert.danger(error.message));
    }
  }

  mounted() {
    articleService
      .getCategories()
      .then(categories => (this.categories = categories))
      .catch((error: Error) => console.log(error.message));

    articleService
      .getArticle(this.props.match.params.id)
      .then(article => (this.article = article))
      .catch((error: Error) => console.log(error.message));
  }
}
