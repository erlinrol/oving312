// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { articleService, Category, Article } from '../services';

//dateformat rounds to minute
function getDate(date) {
  var dateObject = new Date(date);
  dateObject.setSeconds(0, 0);
  return dateObject
    .toISOString()
    .replace('T', ' ')
    .replace(':00.000Z', '');
}

export class ArticleInfo extends Component<{ match: { params: { id: number } } }> {
  // $FlowFixMe flowbug: doesn't understand article is Article without initial typechecking
  article: Article = null;

  render() {
    if (!this.article) return null;

    return (
      <div id="article-info">
        <img id="article-info-picture" src={this.article.picture} alt="Article front image" />
        <div id="article-info-top-wrapper">
          <h6 id="article-info-category"> {this.article.category} </h6>
          <h1 id="article-info-title">{this.article.title}</h1>
          <h6> ({getDate(this.article.createdAt)}) </h6>
        </div>
        <div id="article-info-content">
          <p>{this.article.content}</p>
        </div>
      </div>
    );
  }

  mounted() {
    articleService
      .getArticle(this.props.match.params.id)
      .then(article => (this.article = article))
      .catch((error: Error) => console.log(error.message));
  }
}
