// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { articleService, Category, Article } from '../services';

//dateformat rounds to minute
function getDate(date) {
  var dateObject = new Date(date);
  dateObject.setSeconds(0, 0);
  return dateObject
    .toISOString()
    .replace('T', ' ')
    .replace(':00.000Z', '');
}

export class Feed extends Component {
  articles: Article[] = [];

  render() {
    return (
      <div id="feed">
        {this.articles.map(article => (
          <NavLink key={article.id} to={'/article/' + article.id}>
            <div className="feed-item">{article.title + ' (' + getDate(article.createdAt) + ')'}</div>
          </NavLink>
        ))}
      </div>
    );
  }

  mounted() {
    articleService
      .getArticles()
      .then(articles => ((this.articles = articles), this.runCarousel()))
      .catch((error: Error) => console.log(error.message));
  }

  runCarousel() {
    $(document).ready(function() {
      // $FlowFixMe Slick is not compatible with flow
      $('#feed').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 2000,
        arrows: false,
        pauseOnHover: true
      });
    });
  }
}
