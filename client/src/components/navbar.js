// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

export class Navbar extends Component {
  render() {
    return (
      <nav id="navbar" className="navbar navbar-dark bg-dark">
        <a id="navbar-title" className="navbar-brand" onClick={() => this.toHome()}>
          RealFakeNews
        </a>
        <form className="form-inline">
          <NavLink to="/register">
            <button className="btn btn-outline-success" type="button">
              Register article
            </button>
          </NavLink>
        </form>
      </nav>
    );
  }

  toHome() {
    history.push('/');
    window.location.reload();
  }
}
