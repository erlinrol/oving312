// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { articleService, Category, Article } from '../services';

import { ArticleCard } from './articleCard';

export class MainContent extends Component {
  articles: Article[] = [];
  categories: Category[] = [];
  showing: Article[] = [];
  filterOpen: boolean = false;

  render() {
    return (
      <div id="main-content-wrapper">
        <div id="side-filter" className="w3-sidebar w3-bar-block w3-card" style={{ display: 'none' }}>
          {this.categories.map(category => (
            <a key={category.id} href="#" className="w3-bar-item w3-button" onClick={() => this.filter(category.type)}>
              {category.type}
            </a>
          ))}
        </div>
        <div id="main-content">
          <div>
            <button id="side-filter-button" className="w3-button w3-teal w3-xlarge" onClick={e => this.open()}>
              &#9776;
            </button>
          </div>
          {this.showing.map(article => (
            <ArticleCard
              key={article.id}
              articleId={article.id}
              title={article.title}
              date={article.createdAt}
              picture={article.picture}
              category={article.category}
            />
          ))}
        </div>
      </div>
    );
  }

  open() {
    let mainContent = $('#main-content');
    let sideFilter = $('#side-filter');
    let filterButton = $('#side-filter-button');
    if (this.filterOpen) {
      mainContent.css('margin-left', '0');
      mainContent.css('padding-right', '10px');
      sideFilter.css('width', '0');
      sideFilter.css('display', 'none');
      filterButton.css('left', '0');
      this.filterOpen = false;
    } else if (!this.filterOpen) {
      mainContent.css('margin-left', '20%');
      mainContent.css('padding-right', '18%');
      sideFilter.css('width', '20%');
      sideFilter.css('display', 'block');
      filterButton.css('left', '20%');
      this.filterOpen = true;
    }
  }

  filter(category: string) {
    let cap = 50;
    console.log(category);
    this.showing = this.articles.filter(article => article.category == category && cap-- > 0);
  }

  mounted() {
    let cap = 50;

    articleService
      .getArticles()
      .then(
        articles => (
          (this.articles = articles), (this.showing = articles.filter(article => cap-- > 0 && article.importance == 1))
        )
      )
      .catch((error: Error) => console.log(error.message));

    articleService
      .getCategories()
      .then(categories => (this.categories = categories))
      .catch((error: Error) => console.log(error.message));

  }
}
