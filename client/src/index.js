// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Alert } from './widgets';
import { articleService, Category, Article } from './services';

import { Navbar } from './components/navbar';
import { Feed } from './components/feed';
import { MainContent } from './components/mainContent';
import { ArticleInfo } from './components/articleInfo';
import { ArticleEdit } from './components/articleEdit';
import { Register } from './components/register';

// Reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let script = document.createElement('script');
  script.src = '/reload/reload.js';
  if (document.body) document.body.appendChild(script);
}

class Home extends Component {
  render() {
    return (
      <div id="home">
        <Alert />
        <Feed />
        <MainContent />
      </div>
    );
  }
}

const root = document.getElementById('root');

function renderRoot() {
  if (root)
    ReactDOM.render(
      <HashRouter>
        <div id="page">
          <Navbar />
          <Route exact path="/" component={Home} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/article/:id" component={ArticleInfo} />
          <Route exact path="/article/:id/edit" component={ArticleEdit} />
        </div>
      </HashRouter>,
      root
    );
}

renderRoot();
