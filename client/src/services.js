// @flow
import axios from 'axios';
axios.interceptors.response.use(response => response.data);

export class Category {
  id: number;
  type: string;
}

export class Article {
  id: number;
  title: string;
  content: string;
  picture: string;
  category: string;
  importance: number;
  createdAt: string;
}

class ArticleService {
  getCategories(): Promise<Category[]> {
    return axios.get('/categories');
  }

  getArticles(): Promise<Article[]> {
    return axios.get('/articles');
  }

  getArticle(id: number): Promise<Article> {
    return axios.get('/articles/' + id);
  }

  createArticle(title: string, content: string, picture: string, category: string, importance: number): Promise<void> {
    return axios
      .post('/articles', {
        title: title,
        content: content,
        picture: picture,
        category: category,
        importance: importance
      })
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  updateArticle(article: Article): Promise<void> {
    return axios.put('/articles', article);
  }

  deleteArticle(id: number): Promise<void> {
    return axios.delete('/articles/' + id);
  }
}

export let articleService = new ArticleService();
